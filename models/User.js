const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const user = new mongoose.Schema({
    fullname: {
        type: String,
        require: true
    },
    userId: {
      type: String,
      require: true
    },
    username: {
        type: String,
    },
    password: {
        type: String,
    },
    roleId: {
        type: String,
    },
    tenantId: {
        type: String,
    },
    phoneNumber:{
      type: String,
      index:true,
      unique:true
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    },
    token: {
        type: String,
        required: true
    }
});

module.exports = User = mongoose.model('user', user);