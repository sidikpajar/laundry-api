const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const categories = new mongoose.Schema({
    categoryName: {
        type: String,
        require: true
    },
    tenantId: {
        type: String,
        require: true
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    },
    modifiedBy: {
      type: String,
  },
});

module.exports = Categories = mongoose.model('categories', categories);