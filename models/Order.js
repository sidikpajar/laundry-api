const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const order = new mongoose.Schema({
    orderId: {
        type: String,
        require: true,
        index: true,
        unique: true
    },
    receivedDate: {
        type: String,
    },
    endDate: {
        type: String,
    },
    userId: {
        type: String,
        require: true,
    },
    customerNumberId: {
        type: String,
        require: true,
    },
    customerAdddressId: {
      type: String,
      require: true,
    },
    status: {
      type: String,
    },
    note: {
      type: String,
    },
    balance: {
      type: Number,
    },
    modifiedBy: {
      type: String,
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    },
    tenantId: {
      type: String,
      require: true
    }
});

module.exports = Order = mongoose.model('order', order);