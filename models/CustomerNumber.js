const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const customerNumber = new mongoose.Schema({
    userId: {
        type: String,
        require: true
    },
    phoneNumber: {
        type: String,
        require: true
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    },
    modifiedBy: {
      type: String,
  },
});

module.exports = CustomerNumber = mongoose.model('customerNumber', customerNumber);