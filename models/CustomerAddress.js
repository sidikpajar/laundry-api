const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const customerAddress = new mongoose.Schema({
    userId: {
        type: String,
        require: true
    },
    address: {
        type: String,
        require: true
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    },
    modifiedBy: {
      type: String,
  },
});

module.exports = CustomerAddress = mongoose.model('customerAddress', customerAddress);