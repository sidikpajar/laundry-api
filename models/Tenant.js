const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const tenant = new mongoose.Schema({
    companyName: {
        type: String,
        require: true
    },
    tenantId: {
      type: String,
      require: true,
      index: true,
      unique: true
    },
    phoneNumber: {
        type: String,
        require: true
    },
    status: {
        type: String,
        require: true
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    }
});

module.exports = Tenant = mongoose.model('tenant', tenant);