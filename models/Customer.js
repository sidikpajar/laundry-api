const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const customer = new mongoose.Schema({
    fullName: {
        type: String,
        require: true
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    },
    modifiedBy: {
      type: String
    },
});

module.exports = Customer = mongoose.model('customer', customer);