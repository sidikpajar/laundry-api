const mongoose = require('mongoose');
const moment = require('moment-timezone');
const dateIndonesia = moment.tz(Date.now(), "Asia/Jakarta");
const categoryItem = new mongoose.Schema({
    categoriesId: {
        type: String,
        require: true
    },
    itemName: {
        type: String,
        require: true
    },
    price: {
      type: Number,
      require: true
    },
    modifiedDate: {
        type: Date,
        default: dateIndonesia
    },
    modifiedBy: {
      type: String,
  },
});

module.exports = CategoryItem = mongoose.model('categoryItem', categoryItem);