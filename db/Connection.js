const mongoose = require('mongoose');
require('dotenv').config();
const URI = process.env.DB_CONNECTION;
const connectDB = () => {
    mongoose.connect(URI,{
        useUnifiedTopology: true,
        useNewUrlParser: true
    });
    mongoose.set('useCreateIndex', true);
    console.log('db mongoose conncected..!');
};

module.exports = connectDB;