const express = require('express');
const User = require('../models/User');
const router = express.Router()

// router.post('/phoneNumber/:phoneNumber', async (req, res) => {
//     try{
//         const RequestFindUser = await User.find({phoneNumber: req.params.phoneNumber});
//         let fullname = RequestFindUser.map((user)=>{return user.fullname});
//         let userId = RequestFindUser.map((user)=>{return user._id});
//         let roleId = RequestFindUser.map((user)=>{return user.roleId});
//         let tenantId = RequestFindUser.map((user)=>{return user.tenantId});
//         const jwt = require('jsonwebtoken');
//         const token = jwt.sign({
//           userId: userId, 
//           fullname: fullname, 
//           roleId: roleId,
//           tenantId:tenantId
//          }, 'laundry');
//         res.json({token});
//     }
//     catch(err){
//         res.json({message: err})
//     }  
// })


router.post('/', async (req, res) => {
  const { phoneNumber } = req.body;
  try{
      const RequestFindUser = await User.find({phoneNumber: phoneNumber});
      let token = RequestFindUser.map((user)=>{return user.token});
      res.json({token});
  }
  catch(err){
      res.json({message: err})
  }  
})

router.post('/checkToken', async (req, res) => {
  const { token } = req.body;
  try{
      const jwt = require('jsonwebtoken');
      var decoded = jwt.verify(token, 'laundry');
      res.send(decoded);
  }
  catch(err){
      res.json({message: err})
  }  
})


module.exports = router;
