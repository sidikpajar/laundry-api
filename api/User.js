const express = require('express');
const User = require('../models/User');
const router = express.Router()

router.post('/', async (req, res) => {
  if (!req.headers.authorization) {
    return res.status(403).json({ error: 'No credentials sent!' });
  }
    const {fullname, userId, username, phoneNumber,password, roleId, tenantId, modifiedDate } = req.body;
    const jwt = require('jsonwebtoken');
    const token = jwt.sign({
      userId: userId,
      roleId: roleId,
      tenantId:tenantId
    }, 'laundry');
    const user = new User({
      userId: userId,
      fullname: fullname,
      username: username,
      phoneNumber:phoneNumber,
      password: password,
      roleId: roleId,
      tenantId: tenantId,
      modifiedDate: modifiedDate,
      token:token,
    });
  try{
      const bearerHeader = req.headers['authorization'];
      const bearer = bearerHeader.split(' ');
      const bearerToken = bearer[1];
      const resultDecodeToken = await User.find({token: bearerToken});
      let userId = resultDecodeToken.map((user)=>{return user._id});
      if (typeof userId != "undefined" && userId != null && userId.length != null && userId.length > 0){
        const RequestSaveUser = await user.save();
        res.json(RequestSaveUser);
      }
      return res.status(403).json({ error: 'No credentials sent!' });   
  }
  catch(err){
      res.json({message: err})
  }  
})

router.get('/', async (req, res) => {
    if (!req.headers.authorization) {
      return res.status(403).json({ error: 'No credentials sent!' });
    }
    try{
        const bearerHeader = req.headers['authorization'];
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        const resultDecodeToken = await User.find({token: bearerToken});
        let userId = resultDecodeToken.map((user)=>{return user._id});
        if (typeof userId != "undefined" && userId != null && userId.length != null && userId.length > 0){
          const RequestFindUser = await User.find();
          res.json(RequestFindUser);
        }
        return res.status(403).json({ error: 'No credentials sent!' }); 
    }
    catch(err){
        res.json({message: err})
    }  
})

// Detail
router.get('/:userId', async (req, res) => {
    try{
        const RequestSpecificUser = await User.find({userId: req.params.userId});
        res.json(RequestSpecificUser);
    }
    catch(err){
        res.json({message: err})
    }  
})


// Delete
router.delete('/:userId', async (req, res) => {
    try{
        const RequestSpecificUser = await User.remove({_id: req.params.userId});
        res.json(RequestSpecificUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Update
router.patch('/:userId', async (req, res) => {
    try{
        const RequestUpdateUser = await User.updateOne(
                {_id: req.params.userId},
                {$set: {
                        fullname: req.body.fullname,
                        username: req.body.username,
                        phoneNumber:req.body.phoneNumber,
                        roleId: req.body.roleId,
                        modifiedDate: req.body.modifiedDate
                    }
                }
            );
        res.json(RequestUpdateUser);
    }
    catch(err){
        res.json({message: err})
    }  
})


module.exports = router;
