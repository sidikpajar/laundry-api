const express = require('express');
const Tenant = require('../models/Tenant');
const router = express.Router()

router.post('/', async (req, res) => {
    const {companyName, tenantId, phoneNumber, status, modifiedDate } = req.body;
    const tenant = new Tenant({
      companyName: companyName,
      tenantId: tenantId,
      phoneNumber: phoneNumber,
      status: status,
      modifiedDate: modifiedDate
    });
    try{
        const RequestSaveUser = await tenant.save();
        res.json(RequestSaveUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/', async (req, res) => {
    try{
        // User = from require('../models/User');
        const RequestFindUser = await Tenant.find();
        res.json(RequestFindUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Detail
router.get('/:id', async (req, res) => {
    try{
        const RequestSpecificUser = await Tenant.findById(req.params.id);
        res.json(RequestSpecificUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Delete
router.delete('/:id', async (req, res) => {
    try{
        const RequestSpecificUser = await Tenant.remove({_id: req.params.id});
        res.json(RequestSpecificUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Update
router.patch('/:id', async (req, res) => {
    try{
        const RequestUpdateUser = await Tenant.updateOne(
                {_id: req.params.id},
                {$set: {
                      companyName: req.body.companyName,
                      phoneNumber: req.body.phoneNumber,
                      status: req.body.status,
                      modifiedDate:req.boddy.modifiedDate
                    }
                }
            );
        res.json(RequestUpdateUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

module.exports = router;
