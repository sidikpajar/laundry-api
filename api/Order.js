const express = require('express');
const Order = require('../models/Order');
const router = express.Router()

router.post('/', async (req, res) => {
    const {orderId, status, tenantId, note, balance, modifiedBy, receivedDate, endDate, userId, customerNumberId, customerAdddressId, modifiedDate } = req.body;
    const user = new User({
        orderId: orderId,
        receivedDate: receivedDate,
        endDate: endDate,
        userId: userId,
        customerNumberId: customerNumberId,
        customerAdddressId: customerAdddressId,
        status: status,
        note: note,
        balance: balance,
        modifiedBy: modifiedBy,
        tenantId:tenantId,
        modifiedDate: modifiedDate
    });
    try{
        const RequestSaveUser = await Order.save();
        res.json(RequestSaveUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/tenantId/:tenantId', async (req, res) => {
  try{
      const RequestFindCategory = await Order.find({tenantId: req.params.tenantId});
      res.json(RequestFindCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

router.get('/', async (req, res) => {
    try{
        const FindOrder = await Order.find();
        res.json(FindOrder);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Detail
router.get('/:id', async (req, res) => {
    try{
        const RequestSpecificUser = await Order.findById(req.params.id);
        res.json(RequestSpecificUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Update
router.patch('/:id', async (req, res) => {
    const {orderId, status, tenantId, note, balance, modifiedBy, receivedDate, endDate, userId, customerNumberId, customerAdddressId, modifiedDate } = req.body;
    try{
        const RequestUpdateUser = await User.updateOne(
                {_id: req.params.id},
                {$set: {
                      receivedDate: receivedDate,
                      endDate: endDate,
                      userId: userId,
                      customerNumberId: customerNumberId,
                      customerAdddressId: customerAdddressId,
                      status: status,
                      note: note,
                      balance: balance,
                      modifiedBy: modifiedBy,
                      tenantId:tenantId,
                      modifiedDate: modifiedDate
                    }
                }
            );
        res.json(RequestUpdateUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

module.exports = router;
