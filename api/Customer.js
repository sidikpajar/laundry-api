const express = require('express');
const Customer = require('../models/Customer');
const router = express.Router()

router.post('/', async (req, res) => {
    const {fullname, modifiedBy, modifiedDate } = req.body;
    const customer = new Customer({
        fullname: fullname,
        modifiedDate: modifiedDate,
        modifiedBy: modifiedBy
    });
    try{
        const RequestSaveUser = await customer.save();
        res.json(RequestSaveUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/', async (req, res) => {
    try{
        const RequestFindUser = await Customer.find();
        res.json(RequestFindUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/phoneNumber/:phoneNumber', async (req, res) => {
  try{
      const regex = req.params.phoneNumber
      var regexPattern = new RegExp(regex);
      const RequestFindCategory = await Customer.find({phoneNumber: regexPattern});
      res.json(RequestFindCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

// Delete
router.delete('/:id', async (req, res) => {
    try{
        const RequestSpecificUser = await Customer.remove({_id: req.params.id});
        res.json(RequestSpecificUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Update
router.patch('/:id', async (req, res) => {
    try{
        const RequestUpdateUser = await Customer.updateOne(
                {_id: req.params.id},
                {$set: {
                        fullname: req.body.fullname,
                        modifiedDate: req.body.modifiedDate,
                        modifiedBy: req.body.modifiedBy
                    }
                }
            );
        res.json(RequestUpdateUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

module.exports = router;
