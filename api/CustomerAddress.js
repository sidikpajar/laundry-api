const express = require('express');
const CustomerAddress = require('../models/CustomerAddress');
const router = express.Router()

router.post('/', async (req, res) => {
    const {userId, address, modifiedDate, modifiedBy } = req.body;
    const customerAddress = new CustomerAddress({
        userId: userId,
        address: address,
        modifiedDate: modifiedDate,
        modifiedBy: modifiedBy
    });
    try{
        const RequestSaveCategory = await customerAddress.save();
        res.json(RequestSaveCategory);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/', async (req, res) => {
    try{
        const RequestFindCategory = await CustomerAddress.find();
        res.json(RequestFindCategory);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/userId/:id', async (req, res) => {
  try{
      const RequestFindCategory = await CustomerAddress.find({userId: req.params.id});
      res.json(RequestFindCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

// Update
router.patch('/:id', async (req, res) => {
    try{
        const RequestUpdateUser = await CustomerAddress.updateOne(
                {_id: req.params.id},
                {$set: {
                      address: req.body.address,
                      modifiedDate: req.body.modifiedDate,
                      modifiedBy: req.body.modifiedBy
                    }
                }
            );
        res.json(RequestUpdateUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Delete
router.delete('/:id', async (req, res) => {
  try{
      const RequestSpecificCategory= await CustomerAddress.remove({_id: req.params.id});
      res.json(RequestSpecificCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

module.exports = router;
