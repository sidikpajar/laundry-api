const express = require('express');
const Categories = require('../models/Categories');
const router = express.Router()

router.post('/', async (req, res) => {
    const {categoryName, tenantId, modifiedDate, modifiedBy } = req.body;
    const categories = new Categories({
        categoryName: categoryName,
        tenantId: tenantId,
        modifiedDate: modifiedDate,
        modifiedBy: modifiedBy
    });
    try{
        const RequestSaveCategory = await categories.save();
        res.json(RequestSaveCategory);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/', async (req, res) => {
    try{
        const RequestFindCategory = await Categories.find();
        res.json(RequestFindCategory);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/tenantId/:tenantId', async (req, res) => {
  try{
      const RequestFindCategory = await Categories.find({tenantId: req.params.tenantId});
      res.json(RequestFindCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

// Update
router.patch('/:categoriesId', async (req, res) => {
    try{
        const RequestUpdateUser = await Categories.updateOne(
                {_id: req.params.categoriesId},
                {$set: {
                      categoryName: req.body.categoryName,
                      modifiedDate: req.body.modifiedDate,
                      modifiedBy: req.body.modifiedBy
                    }
                }
            );
        res.json(RequestUpdateUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Delete
router.delete('/:categoriesId', async (req, res) => {
  try{
      const RequestSpecificCategory= await Categories.remove({_id: req.params.categoriesId});
      res.json(RequestSpecificCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

module.exports = router;
