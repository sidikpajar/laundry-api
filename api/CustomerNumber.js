const express = require('express');
const CustomerNumber = require('../models/CustomerNumber');
const router = express.Router()

router.post('/', async (req, res) => {
    const {userId, phoneNumber, modifiedDate, modifiedBy } = req.body;
    const customerNumber = new CustomerNumber({
        userId: userId,
        phoneNumber: phoneNumber,
        modifiedDate: modifiedDate,
        modifiedBy: modifiedBy
    });
    try{
        const RequestSaveCategory = await customerNumber.save();
        res.json(RequestSaveCategory);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/', async (req, res) => {
    try{
        const RequestFindCategory = await CustomerNumber.find();
        res.json(RequestFindCategory);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/userId/:id', async (req, res) => {
  try{
      const RequestFindCategory = await CustomerNumber.find({userId: req.params.id});
      res.json(RequestFindCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

// Update
router.patch('/:id', async (req, res) => {
    try{
        const RequestUpdateUser = await CustomerNumber.updateOne(
                {_id: req.params.id},
                {$set: {
                      phoneNumber: req.body.phoneNumber,
                      modifiedDate: req.body.modifiedDate,
                      modifiedBy: req.body.modifiedBy
                    }
                }
            );
        res.json(RequestUpdateUser);
    }
    catch(err){
        res.json({message: err})
    }  
})

// Delete
router.delete('/:id', async (req, res) => {
  try{
      const RequestSpecificCategory= await CustomerNumber.remove({_id: req.params.id});
      res.json(RequestSpecificCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

module.exports = router;
