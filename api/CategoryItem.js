const express = require('express');
const CategoryItem = require('../models/CategoryItem');
const router = express.Router()

router.post('/', async (req, res) => {
    const {categoriesId, itemName, price, modifiedDate, modifiedBy } = req.body;
    const categoryItem = new CategoryItem({
        categoriesId: categoriesId,
        itemName: itemName,
        price: price,
        modifiedDate: modifiedDate,
        modifiedBy: modifiedBy
    });
    try{
        const RequestSave = await categoryItem.save();
        res.json(RequestSave);
    }
    catch(err){
        res.json({message: err})
    }  
})

router.get('/', async (req, res) => {
  try{
      const RequestFindCategory = await CategoryItem.find();
      res.json(RequestFindCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

router.get('/categoriesId/:categoriesId', async (req, res) => {
  try{
      const RequestFindCategory = await CategoryItem.find({categoriesId: req.params.categoriesId});
      res.json(RequestFindCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})


// Update
router.patch('/:id', async (req, res) => {
  try{
      const RequestUpdateUser = await CategoryItem.updateOne(
              {_id: req.params.id},
              {$set: {
                    itemName: req.body.itemName,
                    price: req.body.price,
                    modifiedDate: req.body.modifiedDate,
                    modifiedBy: req.body.modifiedBy
                  }
              }
          );
      res.json(RequestUpdateUser);
  }
  catch(err){
      res.json({message: err})
  }  
})

// Delete
router.delete('/:id', async (req, res) => {
  try{
      const RequestSpecificCategory= await CategoryItem.remove({_id: req.params.id});
      res.json(RequestSpecificCategory);
  }
  catch(err){
      res.json({message: err})
  }  
})

module.exports = router;
