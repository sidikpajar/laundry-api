const express = require('express');
const connectDB = require('./db/Connection');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
require('dotenv').config();

// Call DB
connectDB();

// Setup Express
app.use(cors());
app.use(express.json({extended: false}));
app.use(bodyParser.json());

// Import Routes

const loginModel = require('./api/Login');
app.use('/api/login', loginModel);


const userModel = require('./api/User');
app.use('/api/user', userModel);


const categoryModel = require('./api/Categories');
app.use('/api/categories', categoryModel);

const categoryPriceModel = require('./api/CategoryItem');
app.use('/api/categoryItem', categoryPriceModel);

const customerModel = require('./api/Customer');
app.use('/api/customer', customerModel);


const TenantModel = require('./api/Tenant');
app.use('/api/tenant', TenantModel);

const CustomerNumberModel = require('./api/CustomerNumber');
app.use('/api/customerNumber', CustomerNumberModel);

const CustomerAddressModel = require('./api/CustomerAddress');
app.use('/api/customerAddressNumber', CustomerAddressModel);

const OrderModel = require('./api/Order');
app.use('/api/order', OrderModel);



// PORT SERVER
const PORT = 9000;
app.listen(PORT, ()=>console.log("Server Started"));
